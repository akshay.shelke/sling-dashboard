import { Card } from 'react-bootstrap';
import { Button } from 'react-foundation';
import TableExecutionResult from './TableExecutionResult';

const Automation = () => {
  return (
    <div>
      <Card>
        <Card.Header className="text-center">
          Latest Automation Results
        </Card.Header>
        <Card.Body>
          <Card.Title>
            Following are the latest Automation build results -
          </Card.Title>
          <TableExecutionResult />
          <Card.Text>Details...</Card.Text>
          <Button variant="primary">HTML Report</Button>
        </Card.Body>
        {/* <Card.Footer className="text-muted">2 days ago</Card.Footer> */}
      </Card>
    </div>
  );
};

export default Automation;
