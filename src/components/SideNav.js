import { useState, useEffect } from 'react';
import { Sidenav, Nav, Dropdown, Toggle } from 'rsuite';
import Dashboard from '@rsuite/icons/Dashboard';
import DeviceOtherIcon from '@rsuite/icons/DeviceOther';
import Gear from '@rsuite/icons/Gear';
import CodeIcon from '@rsuite/icons/Code';
import InfoRoundIcon from '@rsuite/icons/InfoRound';
import TableColumnIcon from '@rsuite/icons/TableColumn';
import 'rsuite/dist/rsuite.min.css';
import { Link, withRouter } from 'react-router-dom';

const SideNav = () => {
  const [expanded, setExpanded] = useState(true);
  //const [activeKey, setActiveKey] = useState('1');
  return (
    <div style={{ width: 240 }}>
      {/* <Toggle md="auto"
        onChange={setExpanded}
        checked={expanded}
        checkedChildren="Expand"
        unCheckedChildren="Collapse"
      /> */}
      <br />
      <Sidenav
        expanded={expanded}
        defaultOpenKeys={['2', '3']}
        //activeKey={activeKey}
        //onSelect={setActiveKey}
      >
        <Sidenav.Body>
          <Nav>
            <Nav.Item eventKey="1" icon={<Dashboard />}>
              <Link to="/">Sling Release Dashboard</Link>
            </Nav.Item>
            <Dropdown
              placement="rightStart"
              eventKey="2"
              title="Platforms"
              icon={<DeviceOtherIcon />}
            >
              <Dropdown.Item eventKey="2-1">
                <Link
                  to={{ pathname: '/Platform', state: { platform: 'fireTV' } }}
                >
                  FireTv
                </Link>
              </Dropdown.Item>
              <Dropdown.Item eventKey="2-2">Roku</Dropdown.Item>
              <Dropdown.Item eventKey="2-3">
                <Link
                  to={{ pathname: '/Platform', state: { platform: 'xbox' } }}
                >
                  Xbox
                </Link>
              </Dropdown.Item>
              <Dropdown.Item eventKey="2-4">Comcast</Dropdown.Item>
              <Dropdown.Item eventKey="2-6">LG</Dropdown.Item>
              <Dropdown.Item eventKey="2-5">Browsers</Dropdown.Item>
            </Dropdown>
            {/* <Dropdown
              placement="rightStart"
              eventKey="3"
              title="Automation"
              icon={<CodeIcon />}
            >
              <Dropdown.Item eventKey="3-1">Functional</Dropdown.Item>
              <Dropdown.Item eventKey="3-2">
                <Link
                  to={{ pathname: '/Automation', state: { platform: 'e2e' } }}
                >
                  End To End
                </Link>
              </Dropdown.Item>
            </Dropdown> */}
            <Dropdown
              placement="rightStart"
              eventKey="3"
              title="Preferences"
              icon={<TableColumnIcon />}
            >
              <Dropdown.Item eventKey="3-1" icon={<Gear/>}>Settings</Dropdown.Item>

              <Dropdown.Item eventKey="3-2" icon={<InfoRoundIcon/>}>About</Dropdown.Item>
            </Dropdown>
          </Nav>
        </Sidenav.Body>
      </Sidenav>
    </div>
  );
};

export default withRouter(SideNav);
