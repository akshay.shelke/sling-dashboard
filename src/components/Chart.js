import React, {Component} from 'react';
import { Bar, Line,Pie } from 'react-chartjs-2';
import {dashboardCard1} from '../constants/Constants';



class Chart extends Component {
    constructor(props) {
        super(props);
        this.graphOptions = {
            scales: {
                 xAxes: [{
                     stacked: true
                 }],
                 yAxes: [{
                     stacked: true
                 }]
             }
        }
        this.state = {
            ChartData:{
                labels:['FireTV','Roku','xBox','JS3ft'],
                datasets: [
                    {   stack: dashboardCard1.stack,
                        label: dashboardCard1.label1,
                        labels:dashboardCard1.labels,
                        data:dashboardCard1.Automated,
                        backgroundColor:dashboardCard1.AutomatedBackgroundColor,
                        //hoverBackgroundColor:dashboardCard1.hoverBackgroundColor
                    },
                      
                    {
                        stack: dashboardCard1.stack,
                        label:dashboardCard1.label2,
                        data:dashboardCard1.Remaining,
                        backgroundColor:dashboardCard1.RemainingBackgroundColor,
                    }
                ]
            }
        
        }
    }

    render() {

        return (
            <div className="chart">
             <Bar data={this.state.ChartData} options={{maintainAspectRatio: false}} legendPosition="bottom"/>
        </div>
        
         )
    }

}
export default Chart;



      