import { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import { List, Button } from 'rsuite';
import SpinnerIcon from '@rsuite/icons/legacy/Spinner';
import GitLabService from '../service/GitLabService';
import TableExecutionResult from './TableExecutionResult';
import ArtifactoryService from '../service/ArtifactoryService';
import deviceModels from '../static_data/device_models.json';
import Table from 'react-bootstrap/Table';

const Platform = () => {
  const location = useLocation();
  const { platform } = location.state;
  const [executionResult, setExecutionResult] = useState();
  const [jobFailed, setJobFailed] = useState();
  const [artifactoryPipelineId, setArtifactoryPipelineId] = useState();
  const [artifactoryLink, setartifactoryLink] = useState();
  const [htmlReportLink, setHtmlReportLink] = useState();

  useEffect(async () => {
    const artifactory_link = ArtifactoryService.getArtifactoryLink(platform);
    setArtifactoryPipelineId();
    setArtifactoryPipelineId(ArtifactoryService.getPipelineId(platform));
    setartifactoryLink(artifactory_link);
    setExecutionResult();
    setJobFailed();
    const response = await GitLabService.fetchAutomationReport(platform);
    if (response && response.status) {
      setJobFailed(response);
      setExecutionResult();
    } else {
      setExecutionResult(response);
      setJobFailed();
      setHtmlReportLink(GitLabService.getHtmlReportLink());
    }
  }, [platform]);
  return (
    <div>
      <div>
        <h2>{platform.toUpperCase()}</h2>
      </div>
      <Card className="m-3">
        <Card.Header className="text-center" as="h5">
          Latest Release Build
        </Card.Header>
        <Card.Body>
          <Card.Text className="text-start">
            Following is the latest Relase build details :{' '}
          </Card.Text>
          <Table striped bordered hover>
          <tbody>
          <tr>
              <td>Last Success Pipeline No</td>
              <td>{artifactoryPipelineId}</td>
            </tr>
            <tr>
              <td>Artifactory Link</td>
              <td>
              <a href={artifactoryLink} target="_blank">
            <Button appearance="primary">Artifactory Link</Button>
          </a>
              </td>
            </tr>
          </tbody>
          </Table>
        </Card.Body>
        {/* <Card.Footer className="text-muted">2 days ago</Card.Footer> */}
      </Card>

      <Card className="m-3">
        <Card.Header className="text-center" as="h5">
          Latest Automation Results
        </Card.Header>
        {executionResult ? (
          <Card.Body>
            <Card.Text className="text-start">
              Following are the latest Automation build results -{' '}
              <b>{executionResult.meta.marge.options.reportPageTitle}</b>
            </Card.Text>
            <TableExecutionResult executionResult={executionResult} />
            <Card.Text>Click below button for mochawesome report:</Card.Text>
            <a href={htmlReportLink} target="_blank">
              <Button appearance="primary">HTML Report</Button>
            </a>
          </Card.Body>
        ) : (
          <Card.Body>
            {jobFailed ? (
              <Card.Text className="text-start">
                {jobFailed.status === 'failed' ? (
                  <>
                    Last job is failed. Status: {jobFailed.status}, Time:
                    {jobFailed.finished_at}
                  </>
                ) : (
                  <>
                    Last pipeline for '{platform}' status is: {jobFailed.status}
                  </>
                )}
                <br></br>
                <a href={jobFailed.web_url} target="_blank">
                  Click here to see pipeline
                </a>
              </Card.Text>
            ) : (
              <SpinnerIcon pulse style={{ fontSize: '2em' }} />
            )}
          </Card.Body>
        )}
        {/* <Card.Footer className="text-muted">2 days ago</Card.Footer> */}
      </Card>

      <Card className="m-3">
        <Card.Header className="text-center" as="h5">
          Device Models
        </Card.Header>
        <Card.Body>
          <Card.Text className="text-start">
            Following are the device models -
          </Card.Text>
          <List size="sm" sortable>
            {deviceModels[platform].map((item, index) => (
              <List.Item key={index} index={index}>
                {item}
              </List.Item>
            ))}
          </List>
        </Card.Body>
        {/* <Card.Footer className="text-muted">2 days ago</Card.Footer> */}
      </Card>
    </div>
  );
};

export default Platform;
