//import logo from '../../images/logo.PNG';
import { withRouter } from 'react-router-dom';
import { Navbar, Nav } from 'rsuite';
import logo from '../logo.png';
import UserInfoIcon from '@rsuite/icons/UserInfo';

const Header = () => {
  return (
    <Navbar>
      <Navbar.Brand></Navbar.Brand>
      <Nav href="/">
        <img className="App-logo" alt="brand-logo" src={logo} href="/"></img>
      </Nav>
      {/* <span style={{display:'inline-block', height: '56px', verticalAlign: 'text-bottom', float: 'center'}}>Dashboard</span> */}
      <Nav pullRight>
        <Nav.Item icon={<UserInfoIcon />}> Profile </Nav.Item>
      </Nav>
    </Navbar>
  );
};

export default withRouter(Header);
