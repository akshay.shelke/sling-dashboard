import { Row, Col, Card } from "react-bootstrap";
import { dashboardData } from "../constants/Constants";
import { dashboardCard1,ExecutionChartData } from "../constants/Constants";
import Chart from "./Chart";
import ExecutionChart from "./ExecutionChart";

const Dashboard = () => {
  return (
    <div className="m-3">
      <br />
      <Row xs={1} md={2} className="g-4">
      <Col key={1}>
            <Card style={{ height: '18rem'}}>
            <Card.Header className="text-center" as="h5">
                {dashboardCard1.title}
            </Card.Header>
            <Card.Body style={{backgroundImage: 'linear-gradient(to bottom right, #f0d5b7 , #067d68)'}}>
                {/* <Card.Text>{item.description}</Card.Text> */}
                <Chart legendPosition='right'/>
            </Card.Body>
            </Card>
        </Col> 
        <Col key={1}>
            <Card style={{ height: '18rem'}}>
            <Card.Header className="text-center" as="h5">
                {ExecutionChartData.title}
            </Card.Header>
            <Card.Body style={{backgroundImage: 'linear-gradient(to bottom right, #fed0c5 , #8b47b5)'}}>
            <ExecutionChart legendPosition='right'/>
                
            </Card.Body>
            </Card>
        </Col>
      </Row>
      <br/>
      <Row xs={1} md={2} className="g-4">
        {dashboardData.map((item, i) => {
          return (
            <Col key={i}>
              <Card style={{ borderRadius: '15px'}}>
                <Card.Header className="text-center" as="h5" style= {{borderRadius: '15px 15px 0px 0px'}}>
                  {item.title}
                </Card.Header>
                <Card.Body style={{ height: '18rem', backgroundImage: item.image, borderRadius: '0px 0px 15px 15px'}}>
                  <Card.Text>{item.description}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          );
        })}
        {/* {dashboardData.map((item, id) => {
              <Col key={id}>
              <Card >
                <Card.Img variant="top" src="holder.js/100px160" />
                <Card.Body>
                  <Card.Title>{item.title}</Card.Title>
                  <Card.Text>{item.description}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          })} */}
      </Row>
    </div>
  );
};

export default Dashboard;
