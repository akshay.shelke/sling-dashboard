import Table from 'react-bootstrap/Table';

const TableExecutionResult = ({ executionResult }) => {
  return (
    <>
      {executionResult && (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Description</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            {/* <tr>
              <td>App Version</td>
              <td id="pipelineid"></td>
            </tr>
            <tr>
              <td>Device Model</td>
              <td id="deviceModel"></td>
            </tr>
            <tr>
              <td>Environment</td>
              <td id="environment"></td>
            </tr> */}
            <tr>
              <td>Tests Executed</td>
              <td id="testExecuted">{executionResult.stats.tests}</td>
            </tr>
            <tr>
              <td>Testcases Passed</td>
              <td id="testPassed">{executionResult.stats.passes}</td>
            </tr>
            <tr>
              <td>Testcases Failed</td>
              <td id="testFailed">{executionResult.stats.failures}</td>
            </tr>
            <tr>
              <td>Tests skipped</td>
              <td id="testSkipped">{executionResult.stats.skipped}</td>
            </tr>
            <tr>
              <td>Failures Defect Skipped</td>
              <td id="defectSkipped">{executionResult.stats.pending}</td>
            </tr>
            <tr>
              <td>Start Time</td>
              <td id="startTime">{executionResult.stats.start}</td>
            </tr>
            <tr>
              <td>End Time</td>
              <td id="endTime">{executionResult.stats.end}</td>
            </tr>
            <tr>
              <td>Duration</td>
              <td id="duration">
                {(executionResult.stats.duration / 3600000).toFixed(2)} Hrs.
              </td>
            </tr>
            <tr>
              <td>Pass Percentage</td>
              <td id="passPercent">
                {executionResult.stats.passPercent.toFixed(2)} %
              </td>
            </tr>
          </tbody>
        </Table>
      )}
    </>
  );
};

export default TableExecutionResult;
