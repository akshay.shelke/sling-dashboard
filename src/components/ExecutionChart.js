import React, {Component} from 'react';
import { Bar, Line,Pie } from 'react-chartjs-2';
import {ExecutionChartData} from '../constants/Constants';



class ExecutionChart extends Component {
    constructor(props) {
        super(props);
        this.graphOptions = {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
            yAxes: [{
                ticks: {
                beginAtZero: true,
                }
                }]
            },
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    formatter: Math.round,
                    font: {
                    weight: 'bold'
                    }
                }
            }
        }
    
        this.execution = {
            ChartData:{
                labels:['FireTV','Roku','xBox','JS3ft'],
                datasets: [
                   
                    {   
                        stack: 'stack1',
                        label: 'Pass',
                        type: 'line',
                        data: ExecutionChartData.TestPassed,
                        backgroundColor:ExecutionChartData.bgColorPassed,
                    },
                    {  
                        stack: 'stack1',
                        label: 'Fail',
                        type: 'line',
                        data: ExecutionChartData.FalseFail,
                        backgroundColor:ExecutionChartData.bgColorFailed,
                    },

                    {  
                        stack: 'stack1',
                        label: 'Stability',
                        type: 'line',
                        data: ExecutionChartData.Stability,
                        backgroundColor:ExecutionChartData.bgColorStability,
                    },

                    {   
                        label: ExecutionChartData.label1,
                        data:ExecutionChartData.TestExecuted,
                        backgroundColor:ExecutionChartData.backgroundColor,
                        
                    }
                ]
            }
        }
    }

    render() {

        return (
            <div className="chart">
             <Bar data={this.execution.ChartData} options={this.graphOptions}/>
        </div>
        
         )
    }

}
export default ExecutionChart;



      