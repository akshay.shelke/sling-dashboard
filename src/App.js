import './App.css';
import 'rsuite/dist/rsuite.min.css';
import SideNav from './components/SideNav';
import MiddleContent from './components/MiddleContent';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Platform from './components/Platform';
import Automation from './components/Automation';
import Header from './components/Header';
import Dashboard from './components/Dashboard';

function App() {
  return (
    <div className="App" style={{ backgroundColor: '#E8E8E8'}}>
      <Router>
        <Header />
        <Row>
          <Col md="auto">
            <SideNav />
          </Col>
          <Col>
            <Switch>
              <Route path="/Platform">
                <Platform />
              </Route>
              <Route path="/Automation">
                <Automation />
              </Route>
              <Route path="/">
                <Dashboard />
              </Route>
              {/* <Route path="/">
            <Platform />
          </Route> */}
            </Switch>
          </Col>
        </Row>
      </Router>
      {/* <Container fluid>
        <Row>
          <Col>
            <SideNav />
          </Col>
          <Col>
            <MiddleContent />
          </Col>
        </Row>
      </Container> */}
    </div>
  );
}

export default App;
