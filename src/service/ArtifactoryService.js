import { atrifactory_base_url } from '../constants/Constants';

class ArtifactoryService {
    getPipelineId(platform) {
        if(platform==='fireTV'){
            return '363850628';
        }else {
            return '393727284';
        }
    }
    getArtifactoryLink(platform) {
        if(platform === 'fireTV'){
            return atrifactory_base_url + this.getPipelineId(platform) + '/' + 'Android';
        }else{
            return atrifactory_base_url + this.getPipelineId(platform) + '/' + platform;
        }
    }
}

export default new ArtifactoryService();