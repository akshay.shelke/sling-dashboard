import axios from 'axios';
import xboxData from '../static_data/mochawesomeXbox.json';
import fireData from '../static_data/mochawesomeFire.json';
import {
  projectAPI,
  privateToken,
  projectId,
  scheduledIds,
  bridgeNames,
  jobNames,
  htmlReportLink,
} from '../constants/Constants';
axios.defaults.headers.common['PRIVATE-TOKEN'] = privateToken;

let finalJobId = 0;
class GitLabService {
  async fetchAutomationReport(platform) {
    if (platform === 'fireTV') {
      return fireData;
    }
    //else if (platform === 'xbox') {
    //   return xboxData;
    // }

    //get pipeline details based on platform passed
    let response = await axios.get(
      `${projectAPI}${projectId}/pipeline_schedules/${scheduledIds[platform]}`
    );
    const lastPipeLineId = await response.data.last_pipeline.id;

    //get pipeline briges for last pipeline
    response = await axios.get(
      `${projectAPI}${projectId}/pipelines/${lastPipeLineId}/bridges`
    );

    //select bridge based on actual bridge name
    const selectedBridge = await response.data.find(
      (bridge) => bridge.name === bridgeNames[platform]
    );

    //fetch jobs from downstream pipelineid
    response = await axios.get(
      `${projectAPI}${projectId}/pipelines/${selectedBridge.downstream_pipeline.id}/jobs`
    );

    //get final job id
    const finalJob = await response.data.find(
      (job) => job.name === jobNames[platform]
    );

    //if job is not succeeded then return
    if (finalJob.status !== 'success') {
      return finalJob;
    } else {
      //fetch artifatcs based on jobid
      finalJobId = finalJob.id;
      response = await axios.get(
        `${projectAPI}${projectId}/jobs/${finalJobId}/artifacts/testing/output/mochawesome.json`
      );
      return response.data;
    }
  }

  getHtmlReportLink() {
    if (finalJobId > 0)
      return `${htmlReportLink}${finalJobId}/artifacts/testing/output/mochawesome.html`;
  }
}

export default new GitLabService();
