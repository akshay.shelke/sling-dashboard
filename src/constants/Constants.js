export const projectAPI = 'https://gitlab.com/api/v4/projects/';
export const privateToken = 'PFPuRT3r75XPw_PSDFUj';
export const projectId = 25159562;

/**Fire TV */
export const scheduledIds = {
  fireTV: 161492,
  xbox: 161075,
};
export const bridgeNames = {
  fireTV: 'Android:build',
  xbox: 'Xbox',
};

export const jobNames = {
  fireTV: 'test-on-device:e2e-run:android-tv',
  xbox: 'xbox:e2e:automation:runtest',
};

export const atrifactory_base_url =
  'https://p-gp2-artifactory.imovetv.com/artifactory/react-native-sling-ui-app-gradle-local/LAT/';

export const htmlReportLink = 'https://dish-cloud.gitlab.io/-/sling/future-ui-team/sling-ui/-/jobs/'
export const dashboardData = [
  {
    title: 'Device Farm',

    color: 'lightgreen',
    image: 'linear-gradient(to bottom right, #fec9c9 , #fd9869)',

    description:
      'Here, data regarding the devices and their usage will be displayed to understnad the requirement of new device and other aspects.',
  },

  {
    title: 'Release Calender',

    color: 'turquoise',
    image: 'linear-gradient(to bottom right, #d7ede1 , #014872)',

    description:
      'Here, the release calender for each platform will be displayed',
  },
];

export const dashboardCard1 = 
  {
    title: 'Test Automation Development Status',
    color: 'lightblue',

    stack: 'stack1',
    label1: "Automated",
    labels:["FireTV","Roku","xBox","JS3ft"],
    Automated:[242,259,190,90],
    AutomatedBackgroundColor:"#43A6C6",
    
    label2: "Remaining",
    Remaining:[13,0,32,145],
    RemainingBackgroundColor:"#ffb6c1"
  }

  export const ExecutionChartData = 
  {
    title: 'Test Automation Execution Status',
    color: 'lightblue',

    stack: 'stack1',
    label1: "TestExecuted",
    labels:["FireTV","Roku","xBox","JS3ft"],
    
    TestExecuted:[125,159,136,52],
    backgroundColor:"#43A6C6",
    
    TestPassed:[95,128,73,32],
    bgColorPassed:"#34eb8c",
  
    TestFailed:[17,31,42,20],
    bgColorFailed:"#eb344c",

    ProductBugs:[12,5,22,0],
    FalseFail:[7,26,63,20],

    Stability:[95.60,83.64,82,61],
    bgColorStability:"#eb34b7"
    
  }